from django.conf.urls import url
from django.contrib import admin
from cards.views import CardList, CardComboList

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^cards/$', CardList.as_view()),
    url(r'^cards/(?P<card_1>[0-9]+)/$', CardList.as_view()),
    url(r'^cards/(?P<card_1>[0-9]+)/(?P<card_2>[0-9]+)/$', CardList.as_view()),
    url(r'^combos/(?P<pk>[0-9]+)/$', CardComboList.as_view()),
]
