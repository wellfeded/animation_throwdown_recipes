from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin
from cards.models import Card, CardCombination


class CardList(ListView):
    #paginate_by = 100
    model = Card
    template_name = 'cards/cards.html'

    def get_queryset(self):
        return Card.objects.exclude(card_set_id=6000).filter(card_set__visible=True)


class CardComboList(SingleObjectMixin, ListView):
    model = CardCombination
    template_name = 'cards/combos.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Card.objects.all())
        return super(CardComboList, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CardComboList, self).get_context_data(**kwargs)
        context['card'] = self.object
        return context

    def get_queryset(self):
        if self.object.combinations.exists():
            return self.object.combinations.all()
        else:
            return self.object.ingredients.all()