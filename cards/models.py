from __future__ import unicode_literals

from django.db import models


class Card(models.Model):
    """
    Model to represent a Card
    """
    android_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True, blank=True)
    rarity = models.IntegerField()
    tv_show = models.ForeignKey('TVShow')
    trait = models.ForeignKey('Trait', null=True, blank=True)
    card_set = models.ForeignKey('CardSet')

    attack_multiplier = models.DecimalField(default=1.0, decimal_places=1, max_digits=2)
    health_multiplier = models.DecimalField(default=1.0, decimal_places=1, max_digits=2)

    def __unicode__(self):
        card_str = self.name
        try:
            # Show level 4 for now as a good estimation of the card.
            level_4 = self.levels.get(level=4)
        except CardLevel.DoesNotExist:
            # If level 4 does not exist, show highest level.
            level_4 = self.levels.latest('level')
        card_str += ' - '

        for cs in level_4.skills.all():
            card_str += '%s(%s)' % (cs.skill.id, cs.x or cs.v)
            if cs.y:
                card_str += '*'
            card_str += ' '
        return card_str

    class Meta:
        ordering = ['android_id']


class CardCombination(models.Model):
    """
    Model to represent the outcome of the combination of 2 cards
    """
    cards = models.ManyToManyField('Card', related_name='combinations')
    results_card = models.ForeignKey('Card', related_name='ingredients')

    def __unicode__(self):
        return ' + '.join([c.name for c in self.cards.all()]) + ' = %s' % self.results_card


class TVShow(models.Model):
    """
    Model to represent one of the TV shows
    """
    name = models.CharField(max_length=35)

    def __unicode__(self):
        return self.name


class CardType(models.Model):
    """
    Model to represent the types of Cards in the game
    """
    name = models.CharField(max_length=35)

    def __unicode__(self):
        return self.name


class Trait(models.Model):
    """
    Model to represent the traits a card can have
    """
    id = models.CharField(max_length=15, primary_key=True)
    name = models.CharField(max_length=35)

    def __unicode__(self):
        return self.name


class Skill(models.Model):
    """
    Model to represent the skills a card can have
    """
    id = models.CharField(max_length=35, primary_key=True)
    name = models.CharField(max_length=35)
    power = models.IntegerField()
    order = models.IntegerField()
    description = models.CharField(max_length=1000, null=True, blank=True)

    def __unicode__(self):
        return self.name


class CardSet(models.Model):
    """
    Model to represent
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=35)
    visible = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name


class CardSkill(models.Model):
    """
    Model to tie a card to its skill, and the attributes associated with that skill
    """
    card_level = models.ForeignKey('CardLevel', related_name='skills')
    skill = models.ForeignKey('Skill', related_name='card_skills')
    p = models.IntegerField(blank=True, null=True)
    v = models.IntegerField(blank=True, null=True)
    # TODO: Rename this this to tv_show_exclusive models.ForeignKey('TVShow', blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    # TODO: Rename this to uses
    x = models.IntegerField(blank=True, null=True)
    trait_exclusive = models.ForeignKey('Trait', blank=True, null=True)

    class Meta:
        ordering = ['card_level__level']

    @property
    def list_display(self):
        display_string = self.skill.name.capitalize()
        if self.x or self.v:
            display_string += ' - %d' % (self.x or self.v)
        if self.trait_exclusive:
            display_string += ' (%s only)' % self.trait_exclusive.name.capitalize()
        return display_string


class CardLevel(models.Model):
    """
    Model that represents a level of a card.
    """
    card = models.ForeignKey('Card', related_name='levels')
    level = models.IntegerField()
    health = models.IntegerField()
    attack = models.IntegerField()

    class Meta:
        ordering = ['level']

    def __unicode__(self):
        return "%s (Level %d)" % (self.card, self.level)