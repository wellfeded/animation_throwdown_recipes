from django.contrib import admin

from .models import Card, CardSkill, CardType, CardCombination, CardLevel, Skill, Trait, TVShow, CardSet


class LevelInline(admin.TabularInline):
    model = CardLevel
    extra = 0


class CardAdmin(admin.ModelAdmin):
    search_fields = ('=id', 'name')
    inlines = (LevelInline,)

admin.site.register(Card, CardAdmin)
admin.site.register(CardSkill)
admin.site.register(CardType)
admin.site.register(CardCombination)
admin.site.register(CardLevel)
admin.site.register(Skill)
admin.site.register(Trait)
admin.site.register(TVShow)
admin.site.register(CardSet)