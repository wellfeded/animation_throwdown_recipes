from .models import Card, CardCombination, Skill, Trait, TVShow, CardSet, CardType
import xml.etree.ElementTree as ET


def load_cards_from_xml(filename='cards.xml'):
    """
    Load cards from xml file

    :param filename: Filename of xml file containing cards information
    :return:

    """
    tree = ET.parse(filename)
    root = tree.getroot()

    # Load Skills
    for skill in root.findall('skillType'):
        # TODO: Preprocess arguments in description
        Skill.objects.create(
            id=skill.find('id').text,
            name=skill.find('name').text,
            power=skill.find('power').text,
            order=skill.find('order').text,
            description=skill.find('short_desc').text,
        )

    # Load Card Sets
    for card_set in root.findall('cardSet'):
        CardSet.objects.create(
            id=card_set.find('id').text,
            name=card_set.find('name').text,
            visible=int(card_set.find('visible').text),
        )

    # Load TV shows
    for tv_show in root.findall('unit_type'):
        TVShow.objects.create(
            id=int(tv_show.find('id').text),
            name=tv_show.find('name').text,
        )

    # Load Traits
    for trait in root.findall('unit_trait'):
        Trait.objects.create(
            id=trait.find('id').text,
            name=trait.find('name').text,
        )

    # Load Cards
    for unit in root.iter('unit'):
        id = int(unit.find('id').text)
        if id >= 10000:
            # FIXME: Attack Multiplier
            card = Card.objects.create(
                android_id=id,
                name=unit.find('name').text,
                description=unit.find('desc').text if unit.find('desc') is not None else '',
                rarity=unit.find('rarity').text,
                tv_show_id=int(unit.find('type').text[0]),
                card_set_id=int(unit.find('set').text),
            )
            trait = unit.find('trait')
            if trait is not None and trait.text:
                card.trait = Trait.objects.get(id=trait.text)
                card.save()
            health = int(unit.find('health').text) if unit.find('health') is not None else 0
            attack = int(unit.find('attack').text) if unit.find('attack') is not None else 0
            last_level = card.levels.create(
                level=1,
                health=health,
                attack=attack,
            )
            running_skills = {}
            for skill_node in unit.findall('skill'):
                skill = Skill.objects.get(id=skill_node.attrib['id'])
                try:
                    y, trait = process_y(skill_node.attrib.get('y'))
                    last_level.skills.create(
                        skill=skill,
                        p=skill_node.attrib.get('p'),
                        v=skill_node.attrib.get('v'),
                        y=y,
                        x=skill_node.attrib.get('x'),
                        trait_exclusive=trait,
                    )
                    running_skills[skill.id] = {
                        'p': skill_node.attrib.get('p'),
                        'v': skill_node.attrib.get('v'),
                        'y': y,
                        'x': skill_node.attrib.get('x'),
                        'trait_exclusive': trait
                    }
                except ValueError:
                    print 'Value error with id %s: %s' % (id, skill_node.attrib)
            for upgrade in unit.iter('upgrade'):
                level = upgrade.find('level').text
                # Deep copy last_level so we can overwrite it
                new_level = last_level
                new_level.pk = None
                new_level.level = level
                if upgrade.find('health') is not None:
                    new_level.health = int(upgrade.find('health').text)
                if upgrade.find('attack') is not None:
                    new_level.attack = int(upgrade.find('attack').text)
                new_level.save()
                for skill_node in upgrade.iter('skill'):
                    y, trait = process_y(skill_node.attrib.get('y'))
                    running_skills[skill_node.attrib['id']] = {
                        'p': skill_node.attrib.get('p'),
                        'v': skill_node.attrib.get('v'),
                        'y': y,
                        'x': skill_node.attrib.get('x'),
                        'trait_exclusive': trait
                    }
                create_skills_for_level(new_level, running_skills)
                last_level = new_level


def process_y(y):
    """
    Helper method to interpret "y" field.  In some scenarios y refers to a trait, and others
    it is a raw value

    :param y: The y field from the cards.xml file
    :return: y: The value for y
             trait: The trait object that corresponds to this card
    """
    if y and not y[0].isdigit():
        trait = Trait.objects.get(id=y)
        y = None
    else:
        trait = None
    return y, trait


def create_skills_for_level(level, skills):
    """
    Create the Skill object for this level of card.

    :param level: Level to create the skill for
    :param skills: Skills to create

    """
    for skill_id, skill_dict in skills.iteritems():
        skill = Skill.objects.get(id=skill_id)
        level.skills.create(
            skill=skill,
            p=skill_dict.get('p'),
            v=skill_dict.get('v'),
            y=skill_dict.get('y'),
            x=skill_dict.get('x'),
            trait_exclusive=skill_dict.get('trait'),
        )


def load_combos_from_xml(filename='combos.xml'):
    """
    Load possible combinations of cards from xml file.
    :param filename:

    """
    tree = ET.parse(filename)
    root = tree.getroot()
    for combo in root.iter('combo'):
        cards = combo.find('cards')
        card_1_pk = cards.attrib.get('card1')
        card_2_pk = cards.attrib.get('card2')
        card_id = combo.find('card_id').text
        cc = CardCombination(results_card_id=int(card_id))
        cc.save()
        cc.cards.add(int(card_1_pk), int(card_2_pk))



